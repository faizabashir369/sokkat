<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CollectionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function () {
    return View::make('welcome');
});
Route::post('login', [CustomerController::class,'loginCustomer']);
Route::post('phone-verification', [CustomerController::class,'phoneVerification']);
Route::post('save-fav', [OrderController::class,'saveFavourite']);
Route::post('remove-fav', [OrderController::class,'removeFavourite']);
Route::post('get-fav', [OrderController::class,'getFavourite']);
Route::post('removeAddress', [CustomerController::class,'deleteCustomerAddress']);
Route::post('removeCustomerAddress', [CustomerController::class,'deleteCustomerAddress']);

Route::post('push-notification', [Controller::class,'pushNotification']);
Route::post('get-notifications', [Controller::class,'getNotificationList']);
Route::post('get-notifications-details', [Controller::class,'getNotificationDetails']);
Route::get('recent-orders/{phone}', [OrderController::class,'getRecentOrders']);
Route::post('logout', [CustomerController::class,'logoutCustomer']);
Route::post('resend-code',[CustomerController::class,'resendVerificationCode']);
Route::post('create-card',[CustomerController::class,'createCustomerGiftCard']);
Route::put('update-profile',[CustomerController::class,'updateProfile']);

Route::post('shipping_rate', [OrderController::class,'getShippingRates']);
Route::post('remove_discount', [OrderController::class,'removeDiscount']);

Route::get('order-status/{id}', [OrderController::class,'getOrderStatus']);
Route::get('get-faqs', [Controller::class,'getFaqs']);
Route::post('register-customer', [CustomerController::class,'registerCustomer']);
Route::get('/getProducts', [ProductController::class,'index']);
Route::get('/getAddressList/{c_id}', [CustomerController::class,'index']);
Route::post('/addProduct', [ProductController::class,'addProduct']);
Route::get('/getCollections', [CollectionController::class,'index']);
Route::post('/addCustomerAddress', [CustomerController::class,'addCustomerAddress']);
Route::put('/updateCustomerAddress', [CustomerController::class,'updateCustomerAddress']);

Route::get('/getCards', [CardController::class,'index']);
Route::post('/removeCard', [CardController::class,'removeCard']);
Route::post('/createOrder', [OrderController::class,'index']);
Route::get('/verifyDiscountCode/{discount_code}/{customer_id}', [OrderController::class,'verifyDiscountCode']);
Route::post('/contactUs', [Controller::class,'contacUs']);
Route::get('/getProfileDetails/{id}', [CustomerController::class,'getCustomerDetails']);
Route::get('/collectionProducts/{c_id}', [CollectionController::class,'collectionProducts']);

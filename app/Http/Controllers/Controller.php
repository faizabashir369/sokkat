<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\SModel;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
     public function contacUs(Request $request)
    {
        
      try
      {
       $result=DB::table('contact_us')->insert(['full_name' => $request->full_name, 'email' => $request->email, 'message' => $request->message, 'subject' => $request->subject]);
       if($result)
       {
         echo json_encode(array(
           'success' => '0',
           'message' => 'Successfuly saved',
          ),JSON_UNESCAPED_UNICODE);
       }
       else
       {
          echo json_encode(array(
           'success' => '0',
           'message' => 'Failed to save',
          ),JSON_UNESCAPED_UNICODE);
       }
      }
      catch(Eexception $e)
      {
            echo json_encode(array(
           'success' => '0',
           'message' => 'Failed to save',
          ),JSON_UNESCAPED_UNICODE);
      }


    }
      public function getFaqs()
      {

        $result = DB::table('faq')->select()->get();
       
        $response = new \stdClass();
        if(count($result)>0)
        {
        
        $response->success="1";
        $response->message="Data Found";
        $response->data=$result;
        }
        else
        {

        $response->success="0";
        $response->message="No Data Found";
        $response->data=$result;

        }
        echo json_encode($response);
      }

      public function pushNotification(Request $request)
      {
        $params=array();
        $device_token=$request->token;

        $title=$request->title;
        $body=$request->body;
        $token=$request->token;

$result=DB::table('announcements')->insert(['title' => $title, 'description' => $request->body, 'recipient' => $request->recipient]);

       
        $registrationIds = $token;
                    #prep the bundle
                    $msg = array
                    (
                        'body'  => $body,
                        'title' => $title,
                        'sound' => 'metallic_beeps.mp3',
                        'badge'=> 1
                    );
        
                    $fields = array
                    (
                        'to'        => $registrationIds,
                        'data'  => $msg
                    );
                    $notification_data=array();
                    $notification_data['title']= $title;
                    $notification_data['body']= $body;
        
                    $data=array();
                    $data['message']['token']=$token;
                    $data['message']['notification']=$notification_data;
                    $data['message']['data']=$params;
                    $fields = array
                    (
                        'to'        => $registrationIds,
                        'data'  => $data
                    );
        
        
                    $fields=json_encode($fields);
                    #API access key from Google API's Console
                    $api_access_key='AAAA-NFwYOA:APA91bHlgOH__PcyQaMcU4Zh15w6kwMvRZivH2tV4kie_09J0q0q69wuE0aW44RTkFPsj880PSTTsJ6ttsWbXkkg9VB868D4uevNJePCthwJ4v6uEx8APCZ-SONAEXT2oWoDz54siZ-B';
                    $headers = array
                    (
                        'Authorization: key=' . $api_access_key,
                        'Content-Type: application/json'
                    );
        
                    #Send Reponse To FireBase Server
                    $ch = curl_init();
                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    curl_setopt( $ch,CURLOPT_POST, true );
                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt( $ch,CURLOPT_POSTFIELDS,  $fields  );
                    $result = curl_exec($ch );
                    curl_close( $ch );
                    #Echo Result Of FireBase Server
                    return $result;
        
                
                

        
          sendGCM($message,$device_token);
      }
      
      public function sendGCM($message, $id) 
        {
        
        
            $url = 'https://fcm.googleapis.com/fcm/send';
        
            $fields = array (
                    'registration_ids' => array (
                            $id
                    ),
                    'data' => array (
                            "message" => $message
                    )
            );
            $fields = json_encode ( $fields );
        
            $headers = array (
                    'Authorization: key=' . "AAAA-NFwYOA:APA91bHlgOH__PcyQaMcU4Zh15w6kwMvRZivH2tV4kie_09J0q0q69wuE0aW44RTkFPsj880PSTTsJ6ttsWbXkkg9VB868D4uevNJePCthwJ4v6uEx8APCZ-SONAEXT2oWoDz54siZ-B",
                    'Content-Type: application/json'
            );
        
            echo $id;
            echo $message;
              echo shell_exec('curl -X POST --header "Authorization: key=AAAA-NFwYOA:APA91bHlgOH__PcyQaMcU4Zh15w6kwMvRZivH2tV4kie_09J0q0q69wuE0aW44RTkFPsj880PSTTsJ6ttsWbXkkg9VB868D4uevNJePCthwJ4v6uEx8APCZ-SONAEXT2oWoDz54siZ-B" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$id.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
            
         }
         public function getNotificationList(Request $request)
         {
          $result=  DB::table('announcements')->where(['recipient' => $request->recipient])->get();
          $response= new \stdClass();

          if(count($result)>0)
          {
            $response->success='1';
            $response->message='Data found';
            foreach($result as $res)
            {

              $response->notifications[]=$res;
            }
             
          }
          else
          {
                 $response->success='0';
                 $response->message='Data Not found';
          }
          echo json_encode($response,JSON_UNESCAPED_UNICODE);
         }
         public function getNotificationDetails(Request $request)
         {
            $result=  DB::table('announcements')->where(['id' => $request->id])->get();
          $response= new \stdClass();

          if(count($result)>0)
          {
            $response->success='1';
            $response->message='Data found';
            foreach($result as $res)
            {
              $response->notification_details[]=$res;
            }
             
          }
          else
          {
                 $response->success='0';
                 $response->message='Data Not found';
          }
          echo json_encode($response,JSON_UNESCAPED_UNICODE);
         }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ProductController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
 public function index()
    {
        $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/products.json',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;

            return $response;
        
    }
    function addProduct(Request $request)
    {
        $items=$request->items;
        $item_array=$items[0];
        $id=$item_array['id'];
        $quantity=$item_array['quantity'];
        $items=[];
        $items['id']=$id;
        $items['quantity']=$quantity;
        
       

        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/cart/add.js',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => json_encode( array('items' => array($items))),
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
    }



}

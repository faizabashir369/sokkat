<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\OrderModel;
use App\Models\FavouriteProduct;


use Illuminate\Support\Facades\DB;

use Illuminate\Routing\Controller as BaseController;

class OrderController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index(Request $request)
    {
      $order = new \stdClass();
      $data = new \stdClass();
      $order_time = date("Y-m-d H:i:s");
      $array= $request->order;
      $line_array=$array['line_items'][0];
      $variant_id= $line_array['variant_id'];
      $quantity= $line_array['quantity'];
      $line_items=[];
      $line_items['variant_id']=$variant_id;
      $line_items['quantity']=$quantity;
      $data->line_items[]=$line_items;
      $order->order=$data;
      $completed='Completed';
      $products=$request->products;
      $customer=$request->customer;
      $shipping_lines=$request->shipping_lines;
      $billing_address=$request->billing_address;
      $shipping_address=$request->shipping_address;
      $products1=$request->products1;
      $customer=$customer;
      $cust_email=$request->cust_email;
      $cust_mob_no = $request->cust_mob_no;
      $cust_name = $request->cust_name;
      $cust_device_id = $request->cust_device_id;
      $cust_device_type = $request->cust_device_type;
      $name= $customer['first_name'];
           
      $discount_codes=$request->discount_codes;
      
      
      
      $discount_code=$discount_codes['code'];
      $discount_amount=$discount_codes['amount'];
      $discount_type=$discount_codes['type'];
      $order_status = 'Order Created';
     
      $total_price = $request->total_price;
      $curl = curl_init();

              curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/orders.json',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>json_encode($order),
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/json'
                ),
              ));

              $response = curl_exec($curl);

              curl_close($curl);

             
              $response1=json_decode($response,true);
             
              $ord= $response1['order'];
              $shopify_order_number=$ord['id'];

              
      $order_array = array(
            "order"=>array(
                "email"=>$cust_email,
                "fulfillment_status"=> "unfulfilled",
                "send_receipt"=> false,
                "send_fulfillment_receipt"=> false,
                "line_items"=>$products,
                "discount_codes"=>$discount_code,
                "billing_address"=>$billing_address,
                "shipping_address"=>$shipping_address,
                "customer"=>$customer,
                "shipping_lines"=>$shipping_lines,
                "tags"=>'delivery'
        
            )
        );
    $payment_type='';
    $id;
    $success;
    $message;
      try {
        $resut_order=  DB::table('orders')->insert([

          'cust_mob_no'=>$cust_mob_no,
          'cust_device_type'=>$cust_device_type,
          'cust_name'=>$cust_name,
          'cust_device_id'=>$cust_device_id,
          'order_status'=>$completed,
          'order_time'=>$order_time,
          'discount_code'=>$discount_code,
          'discount_amount'=>$discount_amount,
          'discount_type'=>$discount_type,
          'cust_email'=>$cust_email,
          'total_price'=>$total_price,
          'shopify_order_number'=>$shopify_order_number,
          'payment_type'=>$payment_type
          ]);
          $id = DB::getPdo()->lastInsertId();
          $success = "1";
          $message="Data Inserted";
      } catch(Exception $e) {
          $success = "0";
          $message="Error occured";
      }
    
   $products1=$products1[0];
   

try {
    $result_products=DB::table('order_products')->insert([

    'order_id'=>$id,
    'product_id'=>$products1['product_id'],
    'product_variant_id'=>$products1['product_variant_id'],
    'product_name'=>$products1['product_name'],
    'product_category'=>$products1['product_category'],
    'product_price'=>$products1['product_price'],
    'product_quanitity'=>$products1['quantity'],
    'product_image'=>$products1['product_image'],
    'note'=>$products1['note'],
    'variant_title'=>$products1['variant_title'],
]);
     
      $success = "1";
      $message="Order Created";
    
    
} catch(Exception $e) {
    $success = "0";
    $message="Order not created";
}
    $response2 = new \stdClass();
    $response2->success=$success;
    $response2->message=$message;
    $response2->data=$response1['order'];

      echo json_encode($response2);
            
              

        
    }
    public function removeDiscount(Request $request)
    {
      
      $discount_code=$request->discount_code;
      $discount_code_id=$request->discount_code_id;
      $customer_id=$request->customer_id;
      $domain1 = "https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2020-04/discount_codes/lookup.json?code=".$discount_code; 
 
          $headers = get_headers($domain1);
           
           $get_http_response_code=substr($headers[0], 9, 3);

      if ( $get_http_response_code == 303 ) {
          $priceRuleURL=substr($headers[18], 10);
          // $priceRuleURL;
          $priceRuleURL= explode("https://sokkat.myshopify.com",$priceRuleURL);
          
      $priceRuleURL="https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com".$priceRuleURL[1];
      $priceRuleURL= explode("/discount_codes",$priceRuleURL);
      $priceRuleURL= $priceRuleURL[0]."/discount_codes/".$discount_code_id.".json";//
        
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => $priceRuleURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'DELETE',
            CURLOPT_HTTPHEADER => array(),
          ));

          $response = curl_exec($curl);

          curl_close($curl);
          echo $response;
    }
    else
    {
      $discount = array('success' => 0,
            'message' => "Not Found",
            'discount_code' => ''

           ); 
           echo json_encode($discount);  
    }

    }
    public function verifyDiscountCode($discount_code,$customer_id)
    {
      $domain1 = "https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2020-04/discount_codes/lookup.json?code=".$discount_code; 
 
          $headers = get_headers($domain1);
           
           $get_http_response_code=substr($headers[0], 9, 3);

      if ( $get_http_response_code == 303 ) {
          $priceRuleURL=substr($headers[18], 10);
          // $priceRuleURL;
          $priceRuleURL= explode("https://sokkat.myshopify.com",$priceRuleURL);
          
          $priceRuleURL="https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com".$priceRuleURL[1];
          $priceRuleURL= explode("/discount_codes",$priceRuleURL);
          $priceRuleURL= $priceRuleURL[0].".json";//
          $priceRuleURL."<br>";
            /*$xml = file_get_contents($priceRuleURL);
            echo $priceRuleURL;*/

          //  Initiate curl
          $ch = curl_init();
          // Will return the response, if false it print the response
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          // Set the url
          curl_setopt($ch, CURLOPT_URL,$priceRuleURL);
          // Execute
          $result=curl_exec($ch);
          // Closing json_encode($result);
          curl_close($ch);

          // $result;
          $result=json_decode($result, true);
          $discount_code=$result['price_rule']['title'];
          $discount_type=$result['price_rule']['value_type'];
          $discount_price=$result['price_rule']['value'];
          $discount_price = ltrim($discount_price, '-');
          $customer_id_array=$result['price_rule']['prerequisite_customer_ids'];

          $discount = array('discount_code' => $discount_code,
            'discount_type' => $discount_type,
            'discount_price' => $discount_price

           ); 
           echo json_encode($discount);  
          }
          else
          {
            
                  $discount = array('discount_code' => "",
                  'discount_type' => "",
                  'discount_price' => ""

                   );
                
                  
                    $message="Discount code is invalid";
                  
                echo json_encode(array("success" => '0',
                                                 "message" => $message,
                                                "discount" => $discount
                                                ),JSON_UNESCAPED_UNICODE);





          }
          
          }
          public function getRecentOrders($phone)
          {
            $orders = OrderModel::where(['order_status' => 'Completed','cust_mob_no' => $phone])->get();
            $response = new \stdClass();
            if(count($orders)>0)
            {
            
            $response->success="1";
            $response->message="Data Found";
            $response->data=$orders;
            }
            else
            {

            $response->success="0";
            $response->message="No Data Found";
            $response->data=$orders;

            }
               echo json_encode($response);
          
          } 
          public function getOrderStatus($id)
          {
            $res=DB::table('orders')->where('id', $id)->first();
            $response = new \stdClass();
            $success;
            $order_status;
             try {
               if($res)
               {
            
               $response->success='1';
               $response->message='Order Found';
               $response->order_status=$res->order_status;
               }
               else
               {
                $response->success='0';
                 $response->message='Order Not Found';
                 $response->order_status='';
               }
            }catch(Exception $e)
            {
               $response->success='0';
               $response->message='Error Occured';
               $response->order_status='';
            }
            echo json_encode($response);
          }
public function getShippingRates(Request $request)
  {
      $delivery_country=$request->delivery_country;
      $domain1 = 'https://' . config('shopify.credential.APIKEY') . ':' . config('shopify.credential.SECRET') . '@' . config('shopify.credential.DOMAIN') . '/admin/api/2021-07/shipping_zones.json'; 

                  //  Initiate curl
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/shipping_zones.json',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
          ));

          $response = curl_exec($curl);

          curl_close($curl);

        $result1=json_decode($response);
  if($delivery_country=='Saudi Arabia')
    {
      $shippingg_zones=$result1->shipping_zones[0];
    }
  elseif($delivery_country=='Bahrain' || $delivery_country=='Kuwait' ||  $delivery_country=='Oman' || $delivery_country=='Qatar' || $delivery_country=='United Arab Emirates')
  {
     $shippingg_zones=$result1->shipping_zones[2];
  }
  else
  {
    $shippingg_zones=$result1->shipping_zones[1];
  }
  foreach($shippingg_zones->price_based_shipping_rates as $p){ 
            $price=$p->price;

         }
        

                  echo json_encode(array("success" => '1',
                                                   "message" => "Shipping Price",
                                                  "shipping_price" =>  $price
                                                  ),JSON_UNESCAPED_UNICODE);
    }
    public function getFavourite(Request $request)
    {
  try
     {
       $result=FavouriteProduct::where(['cust_mob_no' => $request->cust_mob_no, 'cust_id' =>  $request->cust_id ])->get();
        $product = new \stdClass();
       if($result && count($result)>0 )
       {
         $product->success="1";
        $product->message="Data found";
        foreach ($result as $res) {

            $product_id=$res->product_id;
           $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/products/'.$product_id.'.json',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);
           
            $response=json_decode($response,true);
           
            $product->favourite_products[]=$response['product'];


        }


          echo json_encode($product,JSON_UNESCAPED_UNICODE);
          
       }
       else
       {
         echo json_encode(array(
          'success' =>0,
          'message' => 'Favourite Product Not Found'
          ),JSON_UNESCAPED_UNICODE);
       }
    }
    catch(Exception $e)
    {
        echo json_encode(array(
          'success' => '0',
          'message' => 'An error ocuured'
          ),JSON_UNESCAPED_UNICODE);
    }
    }
    public function removeFavourite(Request $request)
    {

    try
    {
       $result=FavouriteProduct::where(['cust_mob_no' => $request->cust_mob_no,'product_id' => $request->product_id, 'cust_id' =>  $request->cust_id ])->delete();
       if($result)
       {
          echo json_encode(array(
          'success' => '1',
          'message' => 'Favourite Product sucessfuly removed'
          ),JSON_UNESCAPED_UNICODE);
          
       }
       else
       {
         echo json_encode(array(
          'success' =>0,
          'message' => 'Favourite Product Not Found'
          ),JSON_UNESCAPED_UNICODE);
       }
    }
    catch(Exception $e)
    {
        echo json_encode(array(
          'success' => '0',
          'message' => 'An error ocuured'
          ),JSON_UNESCAPED_UNICODE);
    }

      
    }
    public function  saveFavourite(Request $request)
    {
       $response = new \stdClass();
       $success;
           
        try {
        $resut_order=  DB::table('favourite_products')->insert([

          'cust_mob_no'=>$request->cust_mob_no,
          'product_id'=>$request->product_id,
          'cust_id'=>$request->cust_id,
          ]);
        
               $response->success='1';
               $response->message='Favourite Product Saved';
               
        }
        catch(Exception $e)
        {
               $response->success='0';
               $response->message='Error Occured';
               
        }
        echo json_encode($response);
    }
    public function getCustomerOrders(Request $request)
    {
      $cust_id=$request->customer_id;

      $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/'.$cust_id.'/orders.json',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
            ),
          ));

          $response = curl_exec($curl);

          curl_close($curl);
          echo $response;
    }
   
}



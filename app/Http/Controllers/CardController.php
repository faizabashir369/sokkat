<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class CardController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index()
    {
        $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/gift_cards.json',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;
                    
    }
    public function removeCard(Request $request)
    {
     
      $cards=$request->gift_card;
      $id=$cards['id'];
      $card['gift_card']=$cards;
   
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/gift_cards/'.$id.'/disable.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>json_encode($card),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
                
    }
   
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Models\RegisteredCustomer;
use App\Models\RegisteredCustomerCard;
use App\Models\LoginRetry;
use App\Models\RegisteredCustomerDevice;
use App\Models\TemporaryCustomer;
use App\Helpers\ErpHelper;
use App\Helpers\ShopifyHelper;
use App\Helpers\ResponseHelper;
use App\Helpers\UnifonicApiHelper;
use App\Helpers\PasswordEncryptionHelper;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;




use Illuminate\Support\Facades\DB;


use Illuminate\Routing\Controller as BaseController;

class CustomerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
 public function index($c_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/'.$c_id.'/addresses.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json')
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        
    }
    public function updateProfile(Request $request)
    {

    
        $validator = Validator::make(
            $request->all(),
            [
                'shopify_customer_id' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
            ]
        );

        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            return ResponseHelper::validationErrorResponse('Validation failed', $err);
        }
       
         $registeredCustomer = RegisteredCustomer::where('shopify_customer_id', $request->shopify_customer_id);
        if($registeredCustomer) {

            $url = 'https://' . config('shopify.credential.APIKEY') . ':' . config('shopify.credential.SECRET') . '@' . config('shopify.credential.DOMAIN') . '/admin/api/2020-07/customers/' . $request->shopify_customer_id . '.json';


            //decrypt password
//        $password = PasswordEncryptionHelper::decryptPassword($password);

            $customer = new \stdClass();
            $customer->id = $request->shopify_customer_id;
            $customer->first_name = $request->first_name;
            $customer->last_name = $request->last_name;
            $customer->email = $request->email;

            $data = new \stdClass();
            $data->customer = $customer;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json; charset=utf-8",
                    "accept: application/json; charset=utf-8",
                    "X-Shopify-Access-Token:" . config('shopify.credential.SECRET') . ""
                ),
            ));


            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $result = json_decode($response, true);

            if (isset($result['customer'])) {

                $registeredCustomer->first_name = $request->first_name;
                $registeredCustomer->last_name = $request->last_name;
                $registeredCustomer->email = $request->email;
                $registeredCustomer->save();


                return ResponseHelper::successResponse(trans('messages.Customer updated successfully'), ['customer' => $result['customer']]);
            } else {
                
                return ResponseHelper::errorResponse(trans('messages.Customer not found'));
            }
        }
        else{
            return ResponseHelper::errorResponse(trans('messages.Customer not found'));
        }

    }

    public function loginCustomer(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'phone' => 'required',
                'password' => 'required',
                'device_id' => 'required',
                'device_type' => 'required',
            ]
        );

        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            return ResponseHelper::validationErrorResponse('Validation failed', $err);
        }

        //encrypt password
        
        $ip = $request->ip();

        //Account Lockout
        $loginRetry = LoginRetry::where(['ip' => $ip])->first();
        if($loginRetry){
            if($loginRetry->retry_count >= 3){
                $to = Carbon::now();
                $from = $loginRetry->updated_at;

                $diff_in_minutes = $to->diffInMinutes($from);
                if($diff_in_minutes < 5){
                    $remainingTime = 5 - $diff_in_minutes;
                    $error = 'Resend code limit exceeded';
                    return ResponseHelper::errorResponse(trans("messages.$error", ['remainingTime' => $remainingTime ]));
                }
                else{
                    $loginRetry->retry_count = 0;
                    $loginRetry->save();
                }
            }
        }
        if(!$loginRetry){
            $loginRetry = new LoginRetry();
        }
         $password = PasswordEncryptionHelper::encryptPassword($request->password);

        $dbCustomer = RegisteredCustomer::where(['phone' => $request->phone, 'password' => $password])->first();
        
        if(!$dbCustomer){

            $loginRetry->ip = $ip;
            $loginRetry->retry_count += 1;
            $loginRetry->save();

            return ResponseHelper::errorResponse(trans('messages.Invalid phone number or password'));
        }

        $accessTokenResponse = ShopifyHelper::createCustomerAccessToken($dbCustomer);

        if($accessTokenResponse['status'] == 'success'){
            $dbCustomer->access_token = $accessTokenResponse['accessToken'];
            $dbCustomer->expire_at = date('Y-m-d H:i:s', strtotime($accessTokenResponse['expiresAt']));
            $dbCustomer->save();
        }

        $unique = $dbCustomer->id + $dbCustomer->phone;
        $sessionKey = Hash::make($unique);

        $customerDevice = RegisteredCustomerDevice::updateOrCreate(['device_id' => $request->device_id, 'device_type' => $request->device_type],
            [
                'registered_customer_id' => $dbCustomer->id,
                'session_key' => $sessionKey,
                'device_id' => $request->device_id,
                'device_type' => $request->device_type,
            ]);

        $loginRetry = LoginRetry::where(['ip' => $ip])->update(['retry_count' => 0]);

        $dbCustomer = RegisteredCustomer::where(['phone' => $request->phone, 'password' => $password])->with('cards', 'devices')->first();
        return ResponseHelper::successResponse(trans('messages.Customer login successfully'), ['session_key' => $sessionKey,'customer' => $dbCustomer]);
    }

    public function addCustomerAddress(Request $request)
    {
         $data= $request->address;
         $address['address']=$data;
         $id= $request->customer_id;
         $curl = curl_init();

            curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/'.$id.'/addresses.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($address),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
   
        public function updateCustomerAddress()
        {
            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/5196184813664/addresses/6402371059808.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PUT',
          CURLOPT_POSTFIELDS =>'{
          "address": {
            "id": 6402371059808,
            "zip": "90210"
          }
        }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        }

        public function deleteCustomerAddress(Request $request)
        {
            $address_id=$request->address_id;
            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/5196184813664/addresses/'.$address_id.'.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'DELETE',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        }
        public function getCustomerDetails($c_id)
        {
             $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/'.$c_id.'.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
           'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

        }

  public function phoneVerification(Request $request)
    {
       
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'phone' => 'required|unique:registered_customers',
                'email' => 'required|unique:registered_customers',
                'password' => 'required'
            ]
        );

        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            if(isset($err['phone']) && $err['phone'][0] == 'The phone has already been taken.'){
                $err['phone'][0] = trans('messages.The phone has already been taken');
            }
            if(isset($err['email']) && $err['email'][0] == 'The email has already been taken.'){
                $err['email'][0] = trans('messages.The email has already been taken');
            }

            return ResponseHelper::validationErrorResponse('Validation failed', $err);
        }

        $verificationCode = mt_rand(100000, 999999);

        $response = UnifonicApiHelper::sendSms($request->phone, $verificationCode);
        
        $result = json_decode($response);
        if(isset($result->success))
        {

       if($result->success != 'true'){
//                dd($result);
              return ResponseHelper::errorResponse($result->message);
         }
            else
        {
                $messageResponseData = $result->data;
              if(isset($messageResponseData->MessageID)){
                    $loyaltyCardNumber = null;
                    if(isset($request->card_no)){
                        $loyaltyCardNumber = $request->card_no;
                    }

                    //encrypt password
                    $password = PasswordEncryptionHelper::encryptPassword($request->password);

                    $temporaryCustomer = TemporaryCustomer::where(['phone' => $request->phone, 'email' => $request->email])->first();
                    if(!$temporaryCustomer){
                        $temporaryCustomer = new TemporaryCustomer();
                    }
                    $temporaryCustomer->first_name = $request->first_name;
                    $temporaryCustomer->last_name = $request->last_name;
                    $temporaryCustomer->email = $request->email;
                    $temporaryCustomer->password = $password;
                    $temporaryCustomer->phone = $request->phone;
                    
                    if(isset($request->address)){
                        $temporaryCustomer->address = $request->address;
                    }
                    
                    
                    $temporaryCustomer->verification_code = $verificationCode;
                    $temporaryCustomer->message_id = "";
                    $temporaryCustomer->message_status = "";
                    $temporaryCustomer->sent_message_count += 1;
                    $temporaryCustomer->save();

                    if($temporaryCustomer){
                        return ResponseHelper::successResponse(trans('messages.Verification code sent successfully'),[]);
                    }
                }
                else{
                    return ResponseHelper::errorResponse('Error in sending message.');
                }
            }
        }
        else{

            return ResponseHelper::errorResponse($result);
        }
        dd($result);
//        return ResponseHelper::successResponse('Tracking response.', $array);
    }

    public function registerCustomer(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'verification_code' => 'required',
            ]
        );

        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            return ResponseHelper::validationErrorResponse('Validation failed', $err);
        }

        $temporaryCustomer = TemporaryCustomer::where(['verification_code' => $request->verification_code])->first();
        if(!$temporaryCustomer){
            return ResponseHelper::errorResponse(trans('messages.Invalid verification code'));
        }
        else{
            $temporaryCustomer->sent_message_count = 0;
            $temporaryCustomer->save();
        }

//        dd($dbCustomer);
//        //find customer in ERP
//        $getCustomerResponse = ErpHelper::getCustomerData($temporaryCustomer->phone);
////        dd($getCustomerResponse);
//
//        $registerCustomerResponse = null;
//
//        if($getCustomerResponse['status'] == 'error'){
//            //If customer does not exist in ERP then create new one.
//            $registerCustomerResponse = ErpHelper::registerCustomer($temporaryCustomer);
//        }
//        else{
//            $registerCustomerResponse = $getCustomerResponse;
//        }

        //Register customer in ERP
       // $registerCustomerResponse = ErpHelper::registerCustomer($temporaryCustomer);


        //if($registerCustomerResponse['status'] == 'success')
         {
            //$customer = $registerCustomerResponse['customer'];
           // $cards = $registerCustomerResponse['cards'];

 $dbCustomer = RegisteredCustomer::where(['phone' => $temporaryCustomer->phone, 'email' => $temporaryCustomer->email])->first();
            if(!$dbCustomer){
                $dbCustomer = new RegisteredCustomer();
            }
            
            $dbCustomer->customer_account = '';
            $dbCustomer->customer_name = '';
            $dbCustomer->first_name = $temporaryCustomer->first_name;
            $dbCustomer->last_name = $temporaryCustomer->last_name;
            $dbCustomer->email = $temporaryCustomer->email;
            $dbCustomer->password = $temporaryCustomer->password;
            
            $dbCustomer->phone = $temporaryCustomer->phone;
            
            $dbCustomer->address = $temporaryCustomer->address;
            
          //  $dbCustomer->save();

         
            $temporaryCustomer->verification_code = 0;
            $temporaryCustomer->save();

        /*    //save customer device
            if(isset($request->device_id) && isset($request->device_type)) {
                $customerDevice = RegisteredCustomerDevice::updateOrCreate(['device_id' => $request->device_id, 'device_type' => $request->device_type],
                    [
                        'registered_customer_id' => $dbCustomer->id,
                        'device_id' => $request->device_id,
                        'device_type' => $request->device_type,
                    ]);
            }
*/
            //create shopify customer
             $shopifyResponse = ShopifyHelper::createCustomer($dbCustomer);
             
//                dd($shopifyResponse);
            if($shopifyResponse['status'] == 'success'){
//                return ($shopifyResponse['customer']);
              
                $shopifyCustomer = $shopifyResponse['customer'];
                $dbCustomer->shopify_customer_id = $shopifyCustomer['id'];
                $dbCustomer->save();


                $accessTokenResponse = ShopifyHelper::createCustomerAccessToken($dbCustomer);
              
                if($accessTokenResponse['status'] == 'success'){
                    $dbCustomer->access_token = $accessTokenResponse['accessToken'];
                    $dbCustomer->expire_at = date('Y-m-d H:i:s', strtotime($accessTokenResponse['expiresAt']));
                    $dbCustomer->save();
                }

                 return ResponseHelper::successResponse(trans('messages.Customer registered successfully'), ['customer' => $dbCustomer]);
            }
            else{

                $searchResponse = null;
                if(isset($shopifyResponse['errors']['email']) && $shopifyResponse['errors']['email'][0] == 'has already been taken'){
                    $searchResponse = ShopifyHelper::searchCustomer('email', $dbCustomer->email);
                }
                elseif (isset($shopifyResponse['errors']['phone']) && $shopifyResponse['errors']['phone'][0] == 'has already been taken'){
                    $searchResponse = ShopifyHelper::searchCustomer('phone', $dbCustomer->phone);
                  
                }
                else{
                    return ResponseHelper::errorResponse($shopifyResponse['errors']);
                }

                if($searchResponse){
                    
                    if($searchResponse['status'] == 'success'){
//                        return ($searchResponse['customers']);

                        $shopifyCustomer = $searchResponse['customers'][0];
                        $dbCustomer->shopify_customer_id = $shopifyCustomer['id'];
                        $dbCustomer->save();

                        $updateCustomerResponse = ShopifyHelper::updateCustomerInfoOnRegister($shopifyCustomer['id'], $dbCustomer->first_name, $dbCustomer->last_name, $dbCustomer->email, $dbCustomer->phone);
                       
                        $accessTokenResponse = ShopifyHelper::createCustomerAccessToken($dbCustomer);
                       
                      return $accessTokenResponse;

                        if($accessTokenResponse['status'] == 'success'){
                            $dbCustomer->access_token = $accessTokenResponse['accessToken'];
                            $dbCustomer->expire_at = date('Y-m-d H:i:s', strtotime($accessTokenResponse['expiresAt']));
                            $dbCustomer->save();
                            echo "token success";
                            return ResponseHelper::successResponse(trans('messages.Customer registered successfully'), ['customer' => $dbCustomer]);
                        }
                       

       /* $dbCustomer = RegisteredCustomer::where(['customer_account' => $customer->CustAccount])->with('cards', 'devices')->first();
     return ResponseHelper::successResponse(trans('messages.Customer registered successfully'), ['customer' => $dbCustomer]); */
                    }
                    else{
                        
                        return ResponseHelper::errorResponse($searchResponse['errors']);
                    }
                }
            }

        }
      


      
        
    }
    public function logoutCustomer(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'device_id' => 'required',
            ]
        );

        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            return ResponseHelper::validationErrorResponse('Validation failed', $err);
        }

        $logout = RegisteredCustomerDevice::where(['device_id' => $request->device_id])->delete();

        return ResponseHelper::successResponse(trans('messages.Customer logout successfully'));

    }
    public function resendVerificationCode(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'phone' => 'required',
            ]
        );

        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            return ResponseHelper::validationErrorResponse('Validation failed', $err);
        }


        $dbCustomer = TemporaryCustomer::where(['phone' => $request->phone])->first();
        if(!$dbCustomer){
            return ResponseHelper::errorResponse(trans('messages.Phone number does not exist'));
        }

        if($dbCustomer->sent_message_count == 3 || $dbCustomer->sent_message_count > 3){
            $to = Carbon::now();
            $from = $dbCustomer->updated_at;

            $diff_in_minutes = $to->diffInMinutes($from);
            if($diff_in_minutes < 5){
                $remainingTime = 5 - $diff_in_minutes;
//                return ResponseHelper::errorResponse("You have exceeded the limit! Please try again after $remainingTime minutes.");

                $error = 'Resend code limit exceeded';
                return ResponseHelper::errorResponse(trans("messages.$error", ['remainingTime' => $remainingTime ]));

            }
        }

        if($dbCustomer->sent_message_count > 0){
            $to = Carbon::now();
            $from = $dbCustomer->updated_at;

            $diff_in_minutes = $to->diffInMinutes($from);
            if($diff_in_minutes < 1){
                return ResponseHelper::errorResponse(trans('messages.Please try again after a minute'));
            }
        }


        $verificationCode = mt_rand(1000, 9999);

        $response = UnifonicApiHelper::sendSms($request->phone, $verificationCode);
        $result = json_decode($response);
        if(isset($result->success)){

            if($result->success != 'true'){
                return ResponseHelper::errorResponse($result->message);
            }
            else{
                $messageResponseData = $result->data;
                if(isset($messageResponseData->MessageID)){
                    $dbCustomer->verification_code = $verificationCode;
                    $dbCustomer->message_id = $messageResponseData->MessageID;
                    $dbCustomer->message_status = $messageResponseData->Status;
                    $dbCustomer->sent_message_count += 1;

                    if($dbCustomer->save()){
                        return ResponseHelper::successResponse(trans('messages.Verification code sent successfully'),[]);
                    }


                }
                else{
                    return ResponseHelper::errorResponse(trans('messages.Error in sending message'));
                }
            }
        }
        else{
            return ResponseHelper::errorResponse($result);
        }

    }
    public function createCustomerGiftCard(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'shopify_customer_id' => 'required',
                'points' => 'required'
            ]
        );

        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            return ResponseHelper::validationErrorResponse('Validation failed', $err);
        }

        $registeredCustomer = RegisteredCustomer::where('shopify_customer_id', $request->shopify_customer_id)->first();

        if($registeredCustomer){
            $createCardResponse = ErpHelper::createCard($registeredCustomer->phone, 2, $request->points);
//        dd($createCardResponse);
            if($createCardResponse['status'] == 'success'){
                $card = $createCardResponse['card'];
                $registeredCustomerCard = RegisteredCustomerCard::updateOrCreate(['card_number' => $card->CardNumber],
                    [
                        'registered_customer_id' => $registeredCustomer->id,
                        'customer_account' => $card->CustAccount,
                        'customer_name' => $card->CustName,
                        'card_number' => $card->CardNumber,
                        'active_points' => $card->ActivePoints,
                        'card_type' => $card->CardType,
                        'card_status' => $card->CardStatus
                    ]);

                return ResponseHelper::successResponse($createCardResponse['message'],['card' => $card]);
            }else{
                return ResponseHelper::errorResponse(trans('messages.'.$createCardResponse['error']));
            }
        }
        else{
            return ResponseHelper::errorResponse(trans('messages.Customer not found'));
        }



    }
}

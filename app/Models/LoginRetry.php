<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginRetry extends Model
{
    protected $fillable = ['ip', 'retry_count'];
}

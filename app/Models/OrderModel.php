<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class OrderModel extends Authenticatable
{
    use HasFactory, Notifiable,  HasApiTokens;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'orders';
    protected $fillable = [
    'cust_mob_no',
    'cust_device_type',
    'cust_name',
    'cust_device_id',
    'order_status',
    'order_time',
    'discount_code',
    'discount_amount',
    'discount_type',
    'cust_email',
    'total_price',
    'shopify_order_number',
    'payment_type'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

}

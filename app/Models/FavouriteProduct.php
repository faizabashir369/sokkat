<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class FavouriteProduct extends Authenticatable
{
    use HasFactory, Notifiable,  HasApiTokens;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
       protected $table = 'favourite_products';
    protected $fillable = [
        'cust_mob_no',
        'product_id',
        'cust_id',
    ];

    
}

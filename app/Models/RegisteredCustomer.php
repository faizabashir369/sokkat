<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisteredCustomer extends Model
{
    protected $fillable = [
        'shopify_customer_id',
        'customer_identification_number',
        'customer_account',
        'customer_name',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'access_token',
        'expire_at',
        'verification_code',
        'message_id',
        'message_status',
        'sent_message_count',
    ];

    public function cards(){
        return $this->hasMany(RegisteredCustomerCard::class, 'registered_customer_id', 'id');
    }

    public function devices(){
        return $this->hasMany(RegisteredCustomerDevice::class, 'registered_customer_id', 'id');
    }

    //Accessors
    public function getCustomerAccountAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getCustomerNameAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getDobAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getPhoneAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getSaudiIdAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getAddressAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getCityAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getCountryAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getLoyaltyCardNumberAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getAccessTokenAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getExpireAtAttribute($value){
        return is_null($value)?'':$value;
    }

    public function getMessageStatusAttribute($value){
        return is_null($value)?'':$value;
    }

}

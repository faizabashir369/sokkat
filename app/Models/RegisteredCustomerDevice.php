<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisteredCustomerDevice extends Model
{
    protected $fillable = [
        'registered_customer_id',
        'session_key',
        'device_id',
        'device_type',
        ];
}

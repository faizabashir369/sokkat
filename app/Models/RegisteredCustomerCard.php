<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisteredCustomerCard extends Model
{
    protected $fillable = [
        'registered_customer_id',
        'customer_account',
        'customer_name',
        'card_number',
        'active_points',
        'card_type',
        'card_status'
    ];
}

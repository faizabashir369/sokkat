<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporaryCustomer extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'address',
        'verification_code',
        'message_id',
        'message_status',
        'sent_message_count'
    ];
}

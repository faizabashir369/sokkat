<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;


class PasswordEncryptionHelper {

    public static function encryptPassword($password){
        // Store the cipher method
        $ciphering = 'AES-128-CBC';

        // Use OpenSSl Encryption method
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = '7626527771011121';

        // Store the encryption key
        $encryption_key = 'Sokkat';

        // Use openssl_encrypt() function to encrypt the data
        $encryption= openssl_encrypt($password, $ciphering, $encryption_key, $options, $encryption_iv);
// Display the encrypted string
        return $encryption;
    }


    public static function decryptPassword($password){
        // Store the cipher method
        $ciphering = 'AES-128-CBC';

        // Use OpenSSl Encryption method
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;
        // Non-NULL Initialization Vector for decryption
        $decryption_iv = '7626527771011121';

        // Store the decryption key
        $decryption_key = 'Sokkat';

        // Use openssl_decrypt() function to decrypt the data 
        
    $decryption = openssl_decrypt ($password, $ciphering, $decryption_key, $options, $decryption_iv);
  
        //

        return $decryption;
    }

}

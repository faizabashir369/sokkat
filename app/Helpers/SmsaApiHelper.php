<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use SmsaSDK\Smsa;


class SmsaApiHelper {


    public static function addShipment($storeOrder,$shipingAddress, $customerObj, $lineItemsCount){

        if($shipingAddress['phone'] != '' || $shipingAddress['phone'] != null){
            $phoneNumber = $shipingAddress['phone'];
        }else{
            $phoneNumber = $customerObj->phone;
        }

        $cashOnDelivery = '0';
        if($storeOrder->payment_gateway == 'Cash on Delivery (COD)'){
            $cashOnDelivery = '1';
        }

        Smsa::key(config('smsa.key'));   // Setting up the SMSA Key

// Since we are not filling all the SECOM method arguments
// as defined in the WSDL, here we are telling SMSA SDK to
// fill the null values by an empty string
        Smsa::nullValues('');

        $shipmentData = [
            'refNo' => $storeOrder->order_id, // shipment reference in your application
            'cName' => $shipingAddress['first_name'].' '.$shipingAddress['last_name'], // customer name
            'cntry' => $shipingAddress['country_code'], // shipment country
            'cCity' => $shipingAddress['city'], // shipment city, try: Smsa::getRTLCities() to get the supported cities
            'cMobile' => $phoneNumber, // customer mobile
            'cAddr1' => $shipingAddress['address1'], // customer address
//            'cAddr2' => 'ALBAWADI DIST, ...detailed address here', // customer address 2
            'shipType' => 'DLV', // shipment type
            'PCs' => $lineItemsCount, // quantity of the shipped pieces
            'cEmail' => $storeOrder->email, // customer email
            'codAmt' => $cashOnDelivery, // payment amount if it's cash on delivery, 0 if not cash on delivery
            'weight' => $storeOrder->total_weight, // pieces weight
//            'itemDesc' => 'Foo Bar', // extra description will be printed
        ];


        $shipment = Smsa::addShipment($shipmentData);

        $awbNumber = $shipment->getAddShipmentResult();

        $status = Smsa::getStatus(['awbNo' => $awbNumber])->getGetStatusResult();

        $data['awb_number'] = $awbNumber;
        $data['status'] = $status;

        return $data;

    }


    public static function cancelShipment($awbNumber, $reason){
        if($reason == ''){
            $reason = 'Canceled by client';
        }
        Smsa::key(config('smsa.key'));
        $result = Smsa::cancelShipment($awbNumber, config('smsa.key'), $reason)->getCancelShipmentResult();
        $status = Smsa::getStatus(['awbNo' => $awbNumber])->getGetStatusResult();

        $data['cancel_response'] = $result;
        $data['status'] = $status;
        return $data;
    }


    public static function getShipmentStatus($awbNumber){

        Smsa::key(config('smsa.key'));
        $status = Smsa::getStatus(['awbNo' => $awbNumber])->getGetStatusResult();
        $data['status'] = $status;
        return $data;
    }


}

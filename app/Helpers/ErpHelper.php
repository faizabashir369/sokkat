<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Log;


class ErpHelper {

    public static function registerCustomer($dbCustomer){

        $saudiId = '';
        if($dbCustomer->saudi_id != null){
            $saudiId = $dbCustomer->saudi_id;
        }

        $address = '';
        if($dbCustomer->address != null){
            $address = $dbCustomer->address;
        }

        $city = '';
        if($dbCustomer->city != null){
            $city = $dbCustomer->city;
        }

        $country = '';
        if($dbCustomer->country != null){
            $country = $dbCustomer->country;
        }

        $loyaltyCardNumber = '';
        if($dbCustomer->loyalty_card_number != null){
            $loyaltyCardNumber = $dbCustomer->loyalty_card_number;
        }

        $phone = str_replace('+', '00', $dbCustomer->phone);


        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                    <soap:Body>
                        <RegisterCustomer xmlns=\"http://tempuri.org/\">
                            <_company>dd</_company>
                            <_firstName>$dbCustomer->first_name</_firstName>
                            <_lastName>$dbCustomer->last_name</_lastName>
                            <_phoneMobile>$phone</_phoneMobile>
                            <_gender>$dbCustomer->gender</_gender>
                            <_email>$dbCustomer->email</_email>
                            <_idNumber>$saudiId</_idNumber>
                            <_address>$address</_address>
                            <_city>$city</_city>
                            <_country>$country</_country>
                            <_birthDate>$dbCustomer->dob</_birthDate>
                            <_cardNumber>$loyaltyCardNumber</_cardNumber>
                        </RegisterCustomer>
                    </soap:Body>
                </soap:Envelope>";


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "2756",
            CURLOPT_URL => "http://46.235.92.185:2756/RSDDIntegrationService/RSDDIntegrationServices.asmx?op=RegisterCustomer",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml;charset=utf-8",
                "soapaction: http://tempuri.org/RegisterCustomer"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $responseData = [];

        if ($err) {
            $responseData['status'] = 'error';
            $responseData['error'] = $err;
        } else {
//            dd($response);
            $content = $response;
            $xml = simplexml_load_string($response);
            $content2 = str_replace(array_map(function ($e) {
                return "$e:";
            }, array_keys($xml->getDocNamespaces())), array(), $content);

            $xml = simplexml_load_string($content2);

            $registerCustomerResult = json_decode($xml->Body->RegisterCustomerResponse->RegisterCustomerResult);

//            dd($registerCustomerResult);

            if ((isset($registerCustomerResult->Result) && $registerCustomerResult->Result == 1) || isset($registerCustomerResult->Data)) {
////                dd($registerCustomerResult->Data);
                $customer = $registerCustomerResult->Data->Customer;
                $cards = $registerCustomerResult->Data->Cards;

                $responseData['status'] = 'success';
                $responseData['message'] = $registerCustomerResult->ResultMessage;
                $responseData['customer'] = $customer;
                $responseData['cards'] = $cards;

            } else {
                $responseData['status'] = 'error';
                $responseData['error'] = $registerCustomerResult->ResultMessage;
            }

        }

        return $responseData;
    }


    public static function getCustomerData($phone){
        $phone = str_replace('+', '00', $phone);

        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                    <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                        <soap:Body>
                            <GetCustomerData xmlns=\"http://tempuri.org/\">
                                <_company>dd</_company>
                                <_phoneMobile>$phone</_phoneMobile>
                                <_withCards>1</_withCards>
                            </GetCustomerData>
                        </soap:Body>
                    </soap:Envelope>";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "2756",
            CURLOPT_URL => "http://46.235.92.185:2756/RSDDIntegrationService/RSDDIntegrationServices.asmx?WSDL=",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml;charset=utf-8",
                "soapaction: http://tempuri.org/GetCustomerData"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $responseData = [];

        if ($err) {
            $responseData['status'] = 'error';
            $responseData['error'] = $err;
        } else {
//            dd($response);
            $content = $response;
            $xml = simplexml_load_string($response);
            $content2 = str_replace(array_map(function ($e) {
                return "$e:";
            }, array_keys($xml->getDocNamespaces())), array(), $content);

            $xml = simplexml_load_string($content2);

            $customerDataResult = json_decode($xml->Body->GetCustomerDataResponse->GetCustomerDataResult);
//            dd($customerDataResult);
            if ($customerDataResult->Result == 1) {
//////                dd($registerCustomerResult->Data);
                $customer = $customerDataResult->Customer;
                $cards = $customerDataResult->CardsList;

                $responseData['status'] = 'success';
                $responseData['message'] = $customerDataResult->ResultMessage;
                $responseData['customer'] = $customer;
                $responseData['cards'] = $cards;

            } else {
                $responseData['status'] = 'error';
                $responseData['error'] = $customerDataResult->ResultMessage;
            }
        }
        return $responseData;

    }


    public static function createCard($phone, $type, $points){

        $phone = str_replace('+', '00', $phone);

        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                    <soap:Body>
                        <AddCardToCustomer xmlns=\"http://tempuri.org/\">
                            <_company>dd</_company>
                            <_phoneMobile>$phone</_phoneMobile>
                            <_cardType>$type</_cardType>
                            <_points>$points</_points>
                        </AddCardToCustomer>
                    </soap:Body>
                </soap:Envelope>";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "2756",
            CURLOPT_URL => "http://46.235.92.185:2756/RSDDIntegrationService/RSDDIntegrationServices.asmx?WSDL=",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml;charset=utf-8",
                "soapaction: http://tempuri.org/AddCardToCustomer"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $responseData = [];

        if ($err) {
            $responseData['status'] = 'error';
            $responseData['error'] = $err;
        } else {
//            dd($response);
            $content = $response;
            $xml = simplexml_load_string($response);
            $content2 = str_replace(array_map(function ($e) {
                return "$e:";
            }, array_keys($xml->getDocNamespaces())), array(), $content);

            $xml = simplexml_load_string($content2);

//            dd($xml);
            $cardsResult = json_decode($xml->Body->AddCardToCustomerResponse->AddCardToCustomerResult);
//            dd($cardsResult);
            if ($cardsResult->Result == 1) {
//                dd($cardsResult->Data);
                $card = $cardsResult->Card;

                $responseData['status'] = 'success';
                $responseData['message'] = $cardsResult->ResultMessage;
                $responseData['card'] = $card;

            } else {
                $responseData['status'] = 'error';
                $responseData['error'] = $cardsResult->ResultMessage;
            }
        }
        return $responseData;

    }


    public static function getCards($phone, $type){

        $phone = str_replace('+', '00', $phone);

        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                    <soap:Body>
                        <GetCards xmlns=\"http://tempuri.org/\">
                            <_company>dd</_company>
                            <_phoneMobile>$phone</_phoneMobile>
                            <_cardType>$type</_cardType>
                        </GetCards>
                    </soap:Body>
                </soap:Envelope>";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "2756",
            CURLOPT_URL => "http://46.235.92.185:2756/RSDDIntegrationService/RSDDIntegrationServices.asmx?WSDL=",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml;charset=utf-8",
                "soapaction: http://tempuri.org/GetCards"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $responseData = [];

        if ($err) {
            $responseData['status'] = 'error';
            $responseData['error'] = $err;
        } else {
//            dd($response);
            $content = $response;
            $xml = simplexml_load_string($response);
            $content2 = str_replace(array_map(function ($e) {
                return "$e:";
            }, array_keys($xml->getDocNamespaces())), array(), $content);

            $xml = simplexml_load_string($content2);

//            dd($xml);
            $cardsResult = json_decode($xml->Body->GetCardsResponse->GetCardsResult);
//            dd($cardsResult);
            if ($cardsResult->Result == 1) {
//                dd($cardsResult->Data);
                $cards = $cardsResult->Cards;

                $responseData['status'] = 'success';
                $responseData['message'] = $cardsResult->ResultMessage;
                $responseData['cards'] = $cards;

            } else {
                $responseData['status'] = 'error';
                $responseData['error'] = $cardsResult->ResultMessage;
            }
        }
        return $responseData;

    }



    public static function getCard($cardNumber){

        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                    <soap:Body>
                        <GetCardData xmlns=\"http://tempuri.org/\">
                          <_company>dd</_company>
                          <_cardNumber>$cardNumber</_cardNumber>
                        </GetCardData>
                      </soap:Body>
                </soap:Envelope>";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "2756",
            CURLOPT_URL => "http://46.235.92.185:2756/RSDDIntegrationService/RSDDIntegrationServices.asmx?WSDL=",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml;charset=utf-8",
                "soapaction: http://tempuri.org/GetCardData"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $responseData = [];

        if ($err) {
            $responseData['status'] = 'error';
            $responseData['error'] = $err;
        } else {
//            dd($response);
            $content = $response;
            $xml = simplexml_load_string($response);
            $content2 = str_replace(array_map(function ($e) {
                return "$e:";
            }, array_keys($xml->getDocNamespaces())), array(), $content);

            $xml = simplexml_load_string($content2);
            $getCardDataResult = json_decode($xml->Body->GetCardDataResponse->GetCardDataResult);
//            dd($getCardDataResult);
            if ($getCardDataResult->Result == 1) {

                $responseData['status'] = 'success';
                $responseData['card'] = $getCardDataResult->Card;

            } else {
                $responseData['status'] = 'error';
                $responseData['error'] = $getCardDataResult->ResultMessage;
            }
        }
        return $responseData;

    }



    public static function transferPoints($sourceCard, $receiverCard, $points){

        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                    <soap:Body>
                        <TransferPoints xmlns=\"http://tempuri.org/\">
                            <_company>dd</_company>
                            <_sourceCard>$sourceCard</_sourceCard>
                            <_recieverCard>$receiverCard</_recieverCard>
                            <_points>$points</_points>
                        </TransferPoints>
                      </soap:Body>
                </soap:Envelope>";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "2756",
            CURLOPT_URL => "http://46.235.92.185:2756/RSDDIntegrationService/RSDDIntegrationServices.asmx?WSDL=",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml;charset=utf-8",
                "soapaction: http://tempuri.org/TransferPoints"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $responseData = [];

        if ($err) {
            $responseData['status'] = 'error';
            $responseData['error'] = $err;
        } else {
//            dd($response);
            $content = $response;
            $xml = simplexml_load_string($response);
            $content2 = str_replace(array_map(function ($e) {
                return "$e:";
            }, array_keys($xml->getDocNamespaces())), array(), $content);

            $xml = simplexml_load_string($content2);
            $transferPointsResult = json_decode($xml->Body->TransferPointsResponse->TransferPointsResult);
//            dd($transferPointsResult);
            if ($transferPointsResult->Result == 1) {

                $responseData['status'] = 'success';
                $responseData['message'] = $transferPointsResult->ResultMessage;

            } else {
                $responseData['status'] = 'error';
                $responseData['error'] = $transferPointsResult->ResultMessage;
            }
        }
        return $responseData;

    }


    public static function updateBalance($cardNumber, $points){

        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                    <soap:Body>
                        <UpdateBalance xmlns=\"http://tempuri.org/\">
                            <_company>dd</_company>
                            <_cardNumber>$cardNumber</_cardNumber>
                            <_points>$points</_points>
                            </UpdateBalance>
                      </soap:Body>
                </soap:Envelope>";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "2756",
            CURLOPT_URL => "http://46.235.92.185:2756/RSDDIntegrationService/RSDDIntegrationServices.asmx?WSDL=",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml;charset=utf-8",
                "soapaction: http://tempuri.org/UpdateBalance"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $responseData = [];

        if ($err) {
            $responseData['status'] = 'error';
            $responseData['error'] = $err;
        } else {
//            dd($response);
            $content = $response;
            $xml = simplexml_load_string($response);
            $content2 = str_replace(array_map(function ($e) {
                return "$e:";
            }, array_keys($xml->getDocNamespaces())), array(), $content);

            $xml = simplexml_load_string($content2);
            $updateBalanceResult = json_decode($xml->Body->UpdateBalanceResponse->UpdateBalanceResult);
//            dd($updateBalanceResult);
            if ($updateBalanceResult->Result == 1) {

                $responseData['status'] = 'success';
                $responseData['message'] = $updateBalanceResult->ResultMessage;

            } else {
                $responseData['status'] = 'error';
                $responseData['error'] = $updateBalanceResult->ResultMessage;
            }
        }
        return $responseData;

    }


}

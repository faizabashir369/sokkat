<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Response;


/**
 * Class AmazonS3Helper
 * @package App\Helpers
 */
class ResponseHelper {


    /**
     * @param $name
     * @param $path
     * @param $source
     * @return string
     */
    public static  function apiKeyRequired() {
        return Response::json(['status'=>'error','code'=>403, 'message' => 'API key is required']);
    }

    public static  function apiKeyInvalid() {
        return Response::json(['status'=>'error','code'=>403, 'message' => 'Invalid API key']);
    }

    public static  function unauthorized() {
        return Response::json(['status'=>'error','code'=>401, 'message' => 'Invalid session key.']);
    }

    public static  function unauthorizedAdmin() {
        return Response::json(['status'=>'error','code'=>401, 'message' => 'Invalid admin access.']);
    }

    public static  function unauthorizedSeller() {
        return Response::json(['status'=>'error','code'=>401, 'message' => 'Invalid seller access.']);
    }

    public static  function unauthorizedBuyer() {
        return Response::json(['status'=>'error','code'=>401, 'message' => 'Invalid buyer access.']);
    }

    public static  function validationErrorResponse($message, $data ) {
        return Response::json(['status'=>'error','code'=>412, 'message' => $message, 'error_obj' => $data]);
    }

    public static  function successResponse($message, $data = []) {
        return Response::json(['status'=>'success','code'=>200, 'message' => $message, 'data' => $data]);
    }


    public static  function errorResponse($message) {
        return Response::json(['status'=>'error','code'=>404, 'message' => $message]);
    }

}

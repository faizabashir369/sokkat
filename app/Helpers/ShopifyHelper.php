<?php
namespace App\Helpers;


use Illuminate\Support\Facades\Log;



class ShopifyHelper {

    public static function searchCustomer($key, $value)
    {
        if($key == 'phone'){
            $value = str_replace('+', '', $value);
        }

        $query = "$key:$value";
        $url = 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/search.json?query='.$query;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
//            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token: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"
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
//        dd($result);

        $data = [];
        if(isset($result['customers'])){
            $data['status'] = 'success';
            $data['customers'] = $result['customers'];
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }
        return $data;

    }

    public static function createCustomer($dbCustomer)
    {
       
        // RegisteredCustomerCard::where(['customer_account' => $dbCustomer->customer_account, 'card_type' => 1])->first();

        $url = 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers.json';

        

        
        $password = PasswordEncryptionHelper::decryptPassword($dbCustomer->password);

        $customer = new \stdClass();
        $customer->first_name = $dbCustomer->first_name;
        $customer->last_name = $dbCustomer->last_name;
        $customer->email = $dbCustomer->email;
        $customer->phone = $dbCustomer->phone;
        $customer->verified_email = true;
        $customer->password = $password;
        $customer->password_confirmation = $password;
        $customer->send_email_welcome = true;
//        $customer->addresses = $addresses;
        
        $data = new \stdClass();
        $data->customer = $customer;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token: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"
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
        
        $data = [];
        if(isset($result['customer'])){
            $data['status'] = 'success';
            $data['customer'] = $result['customer'];
        }
        else{
            $data['status'] = 'error';
            $data['shopify_errors'] = $result['errors'];
        }
        return $data;

    }


    public static function updateCustomerInfoOnRegister($shopifyCustomerId, $firstName, $lastName, $email, $phone)
    {
         $url = 'https://6c5c2497e275bd6bb69c5b3ac832f843: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@sokkat.myshopify.com/admin/api/2021-07/customers/'.$shopifyCustomerId.'.json';


        //decrypt password
//        $password = PasswordEncryptionHelper::decryptPassword($password);

        $customer = new \stdClass();
        $customer->id = $shopifyCustomerId;
        $customer->first_name = $firstName;
        $customer->last_name = $lastName;
        $customer->email = $email;
        $customer->phone = $phone;

        $data = new \stdClass();
        $data->customer = $customer;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token: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"
            ),
        ));


        $response = curl_exec($curl);
       
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
        
        $data = [];
        if(isset($result['customer'])){
            $data['status'] = 'success';
            $data['customer'] = $result['customer'];
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }

        return $data;

    }



    public static function updateCustomer($dbCustomer, $shopifyCustomerId)
    {
        $loyaltyCard = RegisteredCustomerCard::where(['customer_account' => $dbCustomer->customer_account, 'card_type' => 1])->first();

        $url = 'https://'.config('shopify.credential.APIKEY').':'.config('shopify.credential.SECRET').'@'.config('shopify.credential.DOMAIN').'/admin/api/2020-07/customers/'.$shopifyCustomerId.'.json';

        $dobMetaField = new \stdClass();
        $dobMetaField->key = 'DOB';
        $dobMetaField->value = $dbCustomer->dob;
        $dobMetaField->value_type = 'string';
        $dobMetaField->namespace = 'global';
        $metaFields[] = $dobMetaField;

        if($loyaltyCard){
            $loyaltyCardMetaField = new \stdClass();
            $loyaltyCardMetaField->key = 'Loyalty card number';
            $loyaltyCardMetaField->value = $loyaltyCard->card_number;
            $loyaltyCardMetaField->value_type = 'string';
            $loyaltyCardMetaField->namespace = 'global';
            $metaFields[] = $loyaltyCardMetaField;
        }

        $gender = '';
        switch ($dbCustomer->gender) {
            case 0:
                $gender = 'Unknown';
                break;
            case 1:
                $gender = 'Male';
                break;
            case 2:
                $gender = 'Female';
                break;
        }

        $genderMetaField = new \stdClass();
        $genderMetaField->key = 'Gender';
        $genderMetaField->value = $gender;
        $genderMetaField->value_type = 'string';
        $genderMetaField->namespace = 'global';
        $metaFields[] = $genderMetaField;

        $address = new \stdClass();
        $address->address1 = $dbCustomer->address;
        $address->city = $dbCustomer->city;
//        $address->province = $dbCustomer->state;
        $address->phone = $dbCustomer->phone;
//        $address->zip = $dbCustomer->zip_code;
        $address->last_name = $dbCustomer->last_name;
        $address->first_name = $dbCustomer->first_name;
        $address->country = $dbCustomer->country;

        $addresses[] = $address;

        //decrypt password
        $password = PasswordEncryptionHelper::decryptPassword($dbCustomer->password);

        $customer = new \stdClass();
        $customer->id = $shopifyCustomerId;
        $customer->first_name = $dbCustomer->first_name;
        $customer->last_name = $dbCustomer->last_name;
        $customer->email = $dbCustomer->email;
        $customer->phone = $dbCustomer->phone;
        $customer->verified_email = true;
        $customer->password = $password;
        $customer->password_confirmation = $password;
        $customer->send_email_welcome = true;
        $customer->addresses = $addresses;
        $customer->metafields = $metaFields;

        $data = new \stdClass();
        $data->customer = $customer;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token:".config('shopify.credential.SECRET').""
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
//        dd($result);

        $data = [];
        if(isset($result['customer'])){
            $data['status'] = 'success';
            $data['customer'] = $result['customer'];
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }
        return $data;

    }



    public static function updateCustomerPassword($dbCustomer, $password)
    {
        $url = 'https://'.config('shopify.credential.APIKEY').':'.config('shopify.credential.SECRET').'@'.config('shopify.credential.DOMAIN').'/admin/api/2020-07/customers/'.$dbCustomer->shopify_customer_id.'.json';


        //decrypt password
//        $password = PasswordEncryptionHelper::decryptPassword($password);

        $customer = new \stdClass();
        $customer->id = $dbCustomer->shopify_customer_id;
        $customer->first_name = $dbCustomer->first_name;
        $customer->last_name = $dbCustomer->last_name;
        $customer->email = $dbCustomer->email;
        $customer->phone = $dbCustomer->phone;
        $customer->verified_email = true;
        $customer->password = $password;
        $customer->password_confirmation = $password;
        $customer->send_email_welcome = true;

        $data = new \stdClass();
        $data->customer = $customer;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token:".config('shopify.credential.SECRET').""
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
//        dd($result);

        $data = [];
        if(isset($result['customer'])){
            $data['status'] = 'success';
            $data['customer'] = $result['customer'];
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }
        return $data;

    }



    public static function createCustomerAccessToken($dbCustomer)
    {
        
        $postData['query'] = 'mutation customerAccessTokenCreate($input: CustomerAccessTokenCreateInput!) {
                      customerAccessTokenCreate(input: $input) {
                        customerAccessToken {
                          accessToken
                          expiresAt
                        }
                        customerUserErrors {
                          code
                          field
                          message
                        }
                      }
                    }
                    ';


        //decrypt password
        $password = PasswordEncryptionHelper::decryptPassword($dbCustomer->password);
//        $postData['query'] = $query;
        $postData['variables'] = ['input' => ['email' => $dbCustomer->email,'password' => $password]];


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://sokkat.myshopify.com/api/2020-07/graphql.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_HTTPHEADER => array(
                "X-Shopify-Storefront-Access-Token:943c3d856981348c1c84e8ec644027f1",
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
          
        curl_close($curl);
        try{
            $result = json_decode($response,true);

//            return $result['data'];
            $data = [];
            $customerAccessTokenCreate = $result['data']['customerAccessTokenCreate'];
//            return $customerAccessTokenCreate;
            if ($customerAccessTokenCreate['customerAccessToken'] != null) {
                $customerAccessToken = $customerAccessTokenCreate['customerAccessToken'];
                $data['status'] = 'success';
                $data['accessToken'] = $customerAccessToken['accessToken'];
                $data['expiresAt'] = $customerAccessToken['expiresAt'];
            } else {
                $errors = $customerAccessTokenCreate['customerUserErrors'];
                $data['status'] = 'error';
                $data['errors'] = $errors[0]['message'];
            }


        }catch (\Exception $e){
            $data['status'] = 'error';
            $data['errors'] = 'Unable to create customer access token.';
        }


        return $data;
    }


    public static function getCustomerAddress($addressDetail)
    {

        $url = 'https://'.config('shopify.credential.APIKEY').':'.config('shopify.credential.SECRET').'@'.config('shopify.credential.DOMAIN').'/admin/api/2020-07/customers/'.$addressDetail->shopify_customer_id.'/addresses.json';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
//            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token:".config('shopify.credential.SECRET').""
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
//        dd($result);

        $data = [];
        if(isset($result['addresses'])){
            $data['status'] = 'success';
            $addresses = [];
            foreach ($result['addresses'] as $address){
                $addressDetail = [];
                foreach ($address as $key => $value){
//                    dd($value);
                    if($value == null){
                        $addressDetail[$key] = '';
                    }
                    else{
                        $addressDetail[$key] = $value;
                    }

                }
                $addresses[] = $addressDetail;

            }

            $data['customer_addresses'] = $addresses;
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }
        return $data;

    }


    public static function createCustomerAddress($addressDetail)
    {

        $url = 'https://'.config('shopify.credential.APIKEY').':'.config('shopify.credential.SECRET').'@'.config('shopify.credential.DOMAIN').'/admin/api/2020-07/customers/'.$addressDetail->shopify_customer_id.'/addresses.json';

        $address = new \stdClass();
        $address->address1 = $addressDetail->address1;
        $address->address2 = $addressDetail->address2;
        $address->city = $addressDetail->city;
        $address->province = $addressDetail->province;
//        $address->phone = $addressDetail->phone;
        $address->zip = $addressDetail->zip;
        $address->last_name = $addressDetail->last_name;
        $address->first_name = $addressDetail->first_name;
        $address->country = $addressDetail->country;

        $data = new \stdClass();
        $data->address = $address;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token:".config('shopify.credential.SECRET').""
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
//        dd($result);

        $data = [];
        if(isset($result['customer_address'])){
            $data['status'] = 'success';

            $addressDetail = [];
            foreach ($result['customer_address'] as $key => $value){
//                    dd($value);
                if($value == null){
                    $addressDetail[$key] = '';
                }
                else{
                    $addressDetail[$key] = $value;
                }

            }

            $data['customer_address'] = $addressDetail;
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }
        return $data;

    }


    public static function updateCustomerAddress($addressDetail)
    {

        $url = 'https://'.config('shopify.credential.APIKEY').':'.config('shopify.credential.SECRET').'@'.config('shopify.credential.DOMAIN').'/admin/api/2020-07/customers/'.$addressDetail->shopify_customer_id.'/addresses/'.$addressDetail->address_id.'.json';

        $address = new \stdClass();
        $address->id = $addressDetail->address_id;
        $address->address1 = $addressDetail->address1;
        $address->address2 = $addressDetail->address2;
        $address->city = $addressDetail->city;
        $address->province = $addressDetail->province;
//        $address->phone = $addressDetail->phone;
        $address->zip = $addressDetail->zip;
        $address->last_name = $addressDetail->last_name;
        $address->first_name = $addressDetail->first_name;
        $address->country = $addressDetail->country;

        $data = new \stdClass();
        $data->address = $address;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token:".config('shopify.credential.SECRET').""
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
//        dd($result);

        $data = [];
        if(isset($result['customer_address'])){
            $data['status'] = 'success';
            $addressDetail = [];
            foreach ($result['customer_address'] as $key => $value){
//                    dd($value);
                if($value == null){
                    $addressDetail[$key] = '';
                }
                else{
                    $addressDetail[$key] = $value;
                }

            }
            $data['customer_address'] = $addressDetail;
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }
        return $data;

    }


    public static function deleteCustomerAddress($addressDetail)
    {

        $url = 'https://'.config('shopify.credential.APIKEY').':'.config('shopify.credential.SECRET').'@'.config('shopify.credential.DOMAIN').'/admin/api/2020-07/customers/'.$addressDetail->shopify_customer_id.'/addresses/'.$addressDetail->address_id.'.json';



        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
//            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "accept: application/json; charset=utf-8",
                "X-Shopify-Access-Token:".config('shopify.credential.SECRET').""
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response,true);
//        dd($result);

        $data = [];
        if(count($result) == 0){
            $data['status'] = 'success';
        }
        else{
            $data['status'] = 'error';
            $data['errors'] = $result['errors'];
        }
        return $data;

    }


}

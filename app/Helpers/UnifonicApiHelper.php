<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use SmsaSDK\Smsa;


class UnifonicApiHelper {

    public static function sendSms($phoneNumber, $verificationCode){
        $postData['AppSid'] = '5POcqMdjoDFU7z9uEyf5Bme3LR1hDm';
        $postData['SenderID'] = 'sokkat';
        $postData['Recipient'] = $phoneNumber;
        $postData['Body'] = "Verification code is  ".$verificationCode;


        $curl = curl_init();

curl_setopt_array($curl, array(
CURLOPT_URL => 'http://basic.unifonic.com/rest/SMS/messages?AppSid=5POcqMdjoDFU7z9uEyf5Bme3LR1hDm&Body=verification%20code%20is%20'.$verificationCode.'&Recipient='.$phoneNumber.'&responseType=JSON&CorrelationID=%2522%2522&baseEncode=true&statusCallback=sent&async=false',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_HTTPHEADER => array(
    'Accept: application/json',
    'Authorization: Basic Og=='
  ),
));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }


    public static function sendResetPasswordSms($phoneNumber, $verificationCode){
        $postData['AppSid'] = 'GofxS5KbkvuZgo7H3ojSjKittKmwFG';
        $postData['SenderID'] = 'sokkat';
        $postData['Recipient'] = $phoneNumber;
        $postData['Body'] = "Your reset password verification code: ".$verificationCode;

          $curl = curl_init();

curl_setopt_array($curl, array(
CURLOPT_URL => 'http://basic.unifonic.com/rest/SMS/messages?AppSid=5POcqMdjoDFU7z9uEyf5Bme3LR1hDm&Body=verification%20code%20is%20'.$verificationCode.'&Recipient='.$phoneNumber.'&responseType=JSON&CorrelationID=%2522%2522&baseEncode=true&statusCallback=sent&async=false',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_HTTPHEADER => array(
    'Accept: application/json',
    'Authorization: Basic Og=='
  ),
));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

}
